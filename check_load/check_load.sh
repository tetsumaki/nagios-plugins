#!/usr/bin/env bash

# check_load.sh by tetsumaki
# blog   : https://blog.tetsumaki.net
# source : https://framagit.org/tetsumaki/nagios-plugins

usage() {
	cat <<- EOF
	Usage: $0 [-m <load|nload>] [-w <5,5,5>] [-c <10,10,10>] [-j] [-p]
	-m is the mode : load or nload
	-w is the warning on 1, 5, 15 minutes. must be less than critical
	-c is the critical on 1, 5, 15 minutes. must be superior than warning
	-j is an optional argument to ignore thresholds and always return ok
	-p is an optional argument to not generate output for rrdtool
	-
	info: load is real load in /proc/loadavg
	info: nload (normalized load) is load in /proc/loadavg calculated : load / cores
	-
	Eq: $0 -m load -w 12,8,4 -c 24,20,16
	Eq: $0 -m load -w 3,2,1 -c 6,5,4 -j
	Eq: $0 -m nload -w 3,2,1 -c 6,5,4
	EOF
	exit 1
}

getinfos() {
    WARN=()

    for WARNS in $(echo "${WARNING}" | awk -F ',' '{ print $1, $2, $3 }'); do
        WARN=("${WARN[@]}" "${WARNS}")
    done

    WARN1="${WARN[0]}"
    WARN5="${WARN[1]}"
    WARN15="${WARN[2]}"

    CRIT=()

    for CRITS in $(echo "${CRITICAL}" | awk -F ',' '{ print $1, $2, $3 }'); do
        CRIT=("${CRIT[@]}" "${CRITS}")
    done

    CRIT1="${CRIT[0]}"
    CRIT5="${CRIT[1]}"
    CRIT15="${CRIT[2]}"

    if awk \
        -v warn1="${WARN1}" -v crit1="${CRIT1}" \
        -v warn5="${WARN5}" -v crit5="${CRIT5}" \
        -v warn15="${WARN15}" -v crit15="${CRIT15}" \
        'BEGIN { exit ( warn1 < crit1 && warn5 < crit5 && warn15 < crit15 ) }'
    then
        usage
    fi

    case "${1}" in
    load)
        LOADS=()

        for LOAD in $(awk '{ print $1, $2, $3 }' /proc/loadavg); do
            LOADS=("${LOADS[@]}" "${LOAD}")
        done

        VAL1="${LOADS[0]}"
        VAL5="${LOADS[1]}"
        VAL15="${LOADS[2]}"
        ;;
    nload)
        CORES="$(grep -cE '^cpu[0-9]+' /proc/stat)"
        PERCENTS=()

        for LOAD in $(awk '{ print $1, $2, $3 }' /proc/loadavg); do
            PERCENTS=("${PERCENTS[@]}" "$(echo "${LOAD}" | awk -v cores="${CORES}" '{ print $1 / cores }')")
        done

        VAL1="${PERCENTS[0]}"
        VAL5="${PERCENTS[1]}"
        VAL15="${PERCENTS[2]}"
        ;;
    esac
}

thresholds() {
    if [ -n "${ALWAYSOK}" ]; then
        MSG='OK:'
        PERFDATAS="${MODE}1=${VAL1};;;0; ${MODE}5=${VAL5};;;0; ${MODE}15=${VAL15};;;0;"
        RETURNCODE=0
    elif awk \
        -v val1="${VAL1}" -v crit1="${CRIT1}" \
        -v val5="${VAL5}" -v crit5="${CRIT5}" \
        -v val15="${VAL15}" -v crit15="${CRIT15}" \
        'BEGIN { exit ( crit1 >= val1 && crit5 >= val5 && crit15 >= val15 ) }'
    then
        MSG='CRITICAL:'
        PERFDATAS="${MODE}1=${VAL1};0:${WARN1};0:${CRIT1};0; ${MODE}5=${VAL5};0:${WARN5};0:${CRIT5};0; ${MODE}15=${VAL15};0:${WARN15};0:${CRIT15};0;"
        RETURNCODE=2
    elif awk \
        -v val1="${VAL1}" -v warn1="${WARN1}" \
        -v val5="${VAL5}" -v warn5="${WARN5}" \
        -v val15="${VAL15}" -v warn15="${WARN15}" \
        'BEGIN { exit ( warn1 >= val1 && warn5 >= val5 && warn15 >= val15 ) }'
    then
        MSG='WARNING:'
        PERFDATAS="${MODE}1=${VAL1};0:${WARN1};0:${CRIT1};0; ${MODE}5=${VAL5};0:${WARN5};0:${CRIT5};0; ${MODE}15=${VAL15};0:${WARN15};0:${CRIT15};0;"
        RETURNCODE=3
    else
        MSG='OK:'
        PERFDATAS="${MODE}1=${VAL1};0:${WARN1};0:${CRIT1};0; ${MODE}5=${VAL5};0:${WARN5};0:${CRIT5};0; ${MODE}15=${VAL15};0:${WARN15};0:${CRIT15};0;"
        RETURNCODE=0
    fi

    if [ "${MODE}" == 'load' ]; then
        TEXT="Load average: ${VAL1}, ${VAL5}, ${VAL15}"
    elif [ "${MODE}" == 'nload' ]; then
        TEXT="Normalized load average: ${VAL1}, ${VAL5}, ${VAL15}"
    else
        TEXT="CPU usage: ${VAL1}%, ${VAL5}%, ${VAL15}%"
    fi

    [[ "${RETURNCODE}" -gt 0 ]] && TEXT="${TEXT} - THRESOLDS: -w ${WARN1},${WARN5},${WARN15} -c ${CRIT1},${CRIT5},${CRIT15}"
    [[ -n "${NORRD}" ]] || TEXT="${TEXT}|${PERFDATAS}"

    echo "${MSG} ${TEXT}"
    exit "${RETURNCODE}"
}

while getopts "m:w:c:jp" OPTION; do
    case "${OPTION}" in
        m)
            MODE="${OPTARG}"
            [[ "${MODE}" =~ ^(load|nload)$ ]] || usage
            ;;
        w)
            WARNING="${OPTARG}"
            [[ "${WARNING}" =~ ^[0-9]+(\.?[0-9]+)?,[0-9]+(\.?[0-9]+)?,[0-9]+(\.?[0-9]+)?$ ]] || usage
            ;;
        c)
            CRITICAL="${OPTARG}"
            [[ "${CRITICAL}" =~ ^[0-9]+(\.?[0-9]+)?,[0-9]+(\.?[0-9]+)?,[0-9]+(\.?[0-9]+)?$ ]] || usage
            ;;
        j)
            ALWAYSOK=1
            ;;
        p)
            NORRD=1
            ;;
        ?)
            usage
            ;;
    esac
done
shift $((OPTIND-1))

[[ -z "${MODE}" ]] || [[ -z "$WARNING" ]] || [[ -z "$CRITICAL" ]] && usage

case "${MODE}" in
    load)
        getinfos load
        thresholds load
        exit $?
        ;;
    nload)
        getinfos nload
        thresholds nload
        exit $?
        ;;
esac
